/*
 * ble_receive.h
 *
 *  Created on: Jun 26, 2021
 *      Author: nprimc
 */

#ifndef INC_BLE_RECEIVE_H_
#define INC_BLE_RECEIVE_H_

#include "ble_receive.h"
#include "cmsis_os.h"
#include "stdint.h"
#include "main.h"



/* STATUS Return values */
#define STATUS_OK				0x00
#define STATUS_INVALID_TYPE		0x01
#define STATUS_INVALID_FIELD	0x02


/* BLE Message types (uart_msg_status.parsed.msg_type) */
#define BLE_EXPEDITED_WRITE 		0x01		//sdo_w_e
#define BLE_EXPEDITED_READ 			0x02		//sdo_r_e
#define BLE_SEGMENTED_WRITE_INIT 	0x03		//sdo_w_s
#define BLE_SEGMENTED_WRITE_MSG 	0x04		//sdo_d_w
#define BLE_SEGMENTED_READ_INIT		0x05		//sdo_r
#define BLE_SEGMENTED_READ_MSG		0x06		//sdo_d_r

/* BLE Command stage */
#define BLE_TODO_START 		0x01	//sdo_ in transparent uart
#define BLE_TODO_TYPE		0x02	// r_e (expedited), r (init) and d_r (block) for reads; w_e, w_s (init) and w_d (block)
#define BLE_TODO_FIELD1		0x03
#define BLE_TODO_FIELD2		0x04	// Fields in message (number of fiels is different for message types)
#define BLE_TODO_FIELD3		0x05
#define BLE_TODO_FIELD4		0x06
#define BLE_TODO_FIELD5		0x07
#define BLE_TODO_C_RETURN	0x09	// wait for carriage return on the end of msg, last field still has to be parsed



typedef struct __parsed_msg{
	uint8_t msg_type;		// BLE Message types
	uint16_t nodeID;
	uint16_t index;
	uint8_t sub;
	uint8_t data[7]; // in segmented max data is 7 (no index and sub)
	uint16_t data_len; //expedited this is 4 or less, in segmented  it can be more
} parsed_msg;


typedef struct __uart_msg_status{
	uint8_t msg_todo; 			// state variable to detect "s" "d" and "o"
	uint8_t msg_todo_counter;	//
	parsed_msg parsed;  // fill this after msg starts
} uart_msg_status;

/******************************************************************************************
 * BLE receive task main function ( while(1) )
 ******************************************************************************************/
void ble_receive(void * argument);

/******************************************************************************************
 * Used for decoding type and fields of different BLE message types
 ******************************************************************************************/
uint8_t parse_field(uart_msg_status * msg_status, uint8_t *field_start, uint8_t field_len);

/******************************************************************************************
 * Wrapper for hex_ascii_to_uin32 and dec_ascii_to_uin32
 ******************************************************************************************/
uint32_t decode_ascii_number(uint8_t *in, uint16_t len);

/******************************************************************************************
 * This function converts from ASCII char array in HEX to an unsigned integer (uint32_t)
 *
 * Example:		{'0',''x','1','3','B','c'} -> (uint32_t) 0x13BC
 ******************************************************************************************/
uint32_t hex_ascii_to_uint32(uint8_t *in, uint16_t len);

/******************************************************************************************
 * This function converts from ASCII char array in DEC to an unsigned integer (uint32_t)
 *
 * Example:		{'1',''2','1','3','7','6'} -> (uint32_t) 121376
 ******************************************************************************************/
uint32_t dec_ascii_to_uint32(uint8_t *in, uint16_t len);


/******************************************************************************************
 * This function converts from ascii array of bytes to byte array
 *	len == number of chars in *in array
 * Example:		{'1','3','B','c'} -> {0x13, 0xBC}
 ******************************************************************************************/
void hex_data_decode(uint8_t *in, uint8_t *out, uint16_t in_len, uint16_t out_len);

#endif /* INC_BLE_RECEIVE_H_ */
