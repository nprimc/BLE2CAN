/*
 * can_transmit.h
 *
 *  Created on: Jun 29, 2021
 *      Author: nprimc
 */

#ifndef INC_CAN_TRANSMIT_H_
#define INC_CAN_TRANSMIT_H_

#include "cmsis_os.h"
#include "ble_receive.h"

typedef struct __can_status{
	uint8_t type_sent;
	uint8_t toggle;
} can_status;


void can_transmit(void * arg);


/************************************************************
 *	CAN_tx adds CANopen data to a CAN frame and sends it
 *	payload is always 8B long
 *
 ************************************************************/
void CAN_tx(CAN_TxHeaderTypeDef *can, uint8_t *payload);

/************************************************************
 *	This function transforms parsed_msg (Index, Sub, Type)
 *	into raw bytes, payload of CAN message
 *
 ************************************************************/
void create_CAN_payload(parsed_msg *parsed, can_status *can, uint8_t *payload_arr);



#endif /* INC_CAN_TRANSMIT_H_ */
