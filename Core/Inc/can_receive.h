/*
 * can_receive.h
 *
 *  Created on: Jul 2, 2021
 *      Author: nprimc
 */

#ifndef INC_CAN_RECEIVE_H_
#define INC_CAN_RECEIVE_H_

#include "main.h"

/***********************************************************************
 *	CAN receive task main function ( while(1) )
 ***********************************************************************/
void can_receive(void * arg);

void uart_send_error(uint32_t error_num);
void uart_send_OK(void);
void data_to_string(uint16_t valid_len, uint8_t *data, uint8_t * string);

uint32_t bytes_to_error_num(uint8_t *data);

typedef struct __can_rx_struct{
	CAN_RxHeaderTypeDef *pCanRxHeader;
	uint8_t *data;						// Pointer to start of data (is always 8B)
} can_rx_struct;



#endif /* INC_CAN_RECEIVE_H_ */
