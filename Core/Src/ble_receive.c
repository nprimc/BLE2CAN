/*
 * ble_receive.c
 *
 *  Created on: Jun 26, 2021
 *      Author: nprimc
 */

#include "ble_receive.h"


/* Queues (from main.c) */
extern QueueHandle_t ble_queueHandle;
extern QueueHandle_t can_txQHandle;
extern QueueHandle_t uart_txQHandle;



void ble_receive(void * argument){
	uint8_t rx_buff[64] = {0};
	uint16_t rx_count = 0;
	BaseType_t retval;
	uart_msg_status msg_status;
	msg_status.msg_todo = BLE_TODO_START;

	while(1){

		xQueueReceive(ble_queueHandle, &(rx_buff[rx_count]), portMAX_DELAY); // add to buffer

		/**************************************************************
		 *	START OF msg parsing
		 **************************************************************/
		switch(msg_status.msg_todo){

			case BLE_TODO_START:
				if(rx_buff[rx_count] == '_' && rx_buff[rx_count-1] == 'o' && rx_buff[rx_count-2] == 'd' && rx_buff[rx_count-3] == 's'){ // start == sdo_
					msg_status.msg_todo = BLE_TODO_TYPE;
					msg_status.msg_todo_counter = 0;
					rx_count++;
					break;
				}
				msg_status.msg_todo_counter++;
				rx_count++;
				break;

			case BLE_TODO_TYPE:
				if(rx_buff[rx_count] == ' ' || rx_buff[rx_count] == '\r'){ 	// type ends with space, type len is variable (\r for sdo_d_r\r !!!)
					parse_field(&msg_status, &(rx_buff[rx_count-msg_status.msg_todo_counter]), msg_status.msg_todo_counter);
					msg_status.msg_todo_counter = 0;
					if(rx_buff[rx_count] == '\r'){							// This is all for sdo_d_r, sed to CAN task
						msg_status.msg_todo = BLE_TODO_START;
						msg_status.msg_todo_counter = 0;
						rx_count = 0;
						break;
					}
					rx_count++;
					break;
				}
				msg_status.msg_todo_counter++;
				rx_count++;
				break;

			case BLE_TODO_FIELD1: /* Wait first comma, increment */
				if(rx_buff[rx_count] == ',' && msg_status.msg_todo_counter != 0){ // end of field detected (field is nonzero len)
					parse_field(&msg_status, &(rx_buff[rx_count-msg_status.msg_todo_counter]), msg_status.msg_todo_counter);
					msg_status.msg_todo_counter = 0;
					rx_count++;
					break;
				}
				msg_status.msg_todo_counter++;
				rx_count++;
				break;

			case BLE_TODO_FIELD2:
				if(rx_buff[rx_count] == ',' && msg_status.msg_todo_counter != 0){ // end of field detected (field is nonzero len)
					parse_field(&msg_status, &(rx_buff[rx_count-msg_status.msg_todo_counter]), msg_status.msg_todo_counter);
					msg_status.msg_todo_counter = 0;
					rx_count++;
					break;
				}
				msg_status.msg_todo_counter++;
				rx_count++;
				break;

			case BLE_TODO_FIELD3:
				if(rx_buff[rx_count] == ',' && msg_status.msg_todo_counter != 0){ // end of field detected (field is nonzero len)
					parse_field(&msg_status, &(rx_buff[rx_count-msg_status.msg_todo_counter]), msg_status.msg_todo_counter);
					msg_status.msg_todo_counter = 0;
					rx_count++;
					break;
				}
				msg_status.msg_todo_counter++;
				rx_count++;
				break;

			case BLE_TODO_FIELD4:
				if(rx_buff[rx_count] == ',' && msg_status.msg_todo_counter != 0){ // end of field detected (field is nonzero len)
					parse_field(&msg_status, &(rx_buff[rx_count-msg_status.msg_todo_counter]), msg_status.msg_todo_counter);
					msg_status.msg_todo_counter = 0;
					rx_count++;
					break;
				}
				msg_status.msg_todo_counter++;
				rx_count++;
				break;

			case BLE_TODO_FIELD5:
				if(rx_buff[rx_count] == ',' && msg_status.msg_todo_counter != 0){ // end of field detected (field is nonzero len)
					parse_field(&msg_status, &(rx_buff[rx_count-msg_status.msg_todo_counter]), msg_status.msg_todo_counter);
					msg_status.msg_todo_counter = 0;
					rx_count++;
					break;
				}
				msg_status.msg_todo_counter++;
				rx_count++;
				break;

			case BLE_TODO_C_RETURN:
				if(rx_buff[rx_count] == '\r'){ // end of msg detected, last field

					parse_field(&msg_status, &(rx_buff[rx_count-msg_status.msg_todo_counter]), msg_status.msg_todo_counter);

					/* Send data to CAN transmit task */
					retval = xQueueSend(can_txQHandle, &(msg_status.parsed), 100);

					/* Prepare to receive new message */
					msg_status.msg_todo = BLE_TODO_START;
					msg_status.msg_todo_counter = 0;
					rx_count = 0;
					break;
				}
				msg_status.msg_todo_counter++;
				rx_count++;
				break;

			default:
				msg_status.msg_todo = BLE_TODO_START;
				msg_status.msg_todo_counter = 0;
				rx_count = 0;
		}

		/**************************************************************
		 *  END OF msg parsing
		 **************************************************************/
		/* Empty the buffer if message has not started */
		if(rx_count > 32 && msg_status.msg_todo == BLE_TODO_START){
			rx_count = 0;
		}

	}
}


uint8_t parse_field(uart_msg_status * msg_status, uint8_t *field_start, uint8_t field_len){
	//		FIELD is DEFINED BY:
	// field_start == pointer to first byte of field (rx_buffer)
	// field_len ~ msg_todo_counter (reset after BLE_TODO_ change)

	/* PARSING MESSAGE TYPE (sdo_XXXX) */
	if(msg_status->msg_todo == BLE_TODO_TYPE){
		if(field_start[0] == 'r' && field_start[1] == ' ' ){									//sdo_r
			msg_status->parsed.msg_type = BLE_SEGMENTED_READ_INIT;
		}
		else if(field_start[0] == 'd' && field_start[2] == 'r'){								//sdo_d_r
			msg_status->parsed.msg_type = BLE_SEGMENTED_READ_MSG;
			msg_status->msg_todo = BLE_TODO_C_RETURN;	//no fields
			return STATUS_OK;
		}
		else if(field_start[0] == 'r' && field_start[2] == 'e'){
					msg_status->parsed.msg_type = BLE_EXPEDITED_READ;
		}
		else if(field_start[0] == 'w' && field_start[2] == 'e'){
							msg_status->parsed.msg_type = BLE_EXPEDITED_WRITE;
		}
		else if(field_start[0] == 'w' && field_start[2] == 's'){
							msg_status->parsed.msg_type = BLE_SEGMENTED_WRITE_INIT;
		}
		else if(field_start[0] == 'd' && field_start[2] == 'w'){
							msg_status->parsed.msg_type = BLE_SEGMENTED_WRITE_MSG;
		}
		else {
			/* Error, invalid type */
			msg_status->msg_todo_counter = 0;
			msg_status->msg_todo = BLE_TODO_START;
			return STATUS_INVALID_TYPE;
		}
		msg_status->msg_todo = BLE_TODO_FIELD1;
		return STATUS_OK;
	}


	switch (msg_status->parsed.msg_type) {
		/******************************************************************************
		 *		PARSE EXPEDITED READ fields
		 ******************************************************************************/
		case BLE_EXPEDITED_READ:
			if(msg_status->msg_todo == BLE_TODO_FIELD1){
				/* field1 is NodeID */
				msg_status->parsed.nodeID = (uint16_t) decode_ascii_number(field_start, field_len);
				msg_status->msg_todo = BLE_TODO_FIELD2;

			}
			else if(msg_status->msg_todo == BLE_TODO_FIELD2){
				/* field2 is Index */
				msg_status->parsed.index = (uint16_t) decode_ascii_number(field_start, field_len);
				msg_status->msg_todo = BLE_TODO_C_RETURN;

			}
			else if(msg_status->msg_todo == BLE_TODO_C_RETURN){
				/* field3 (last field before '\r') is Sub */
				msg_status->parsed.sub = (uint8_t) decode_ascii_number(field_start, field_len);
				msg_status->msg_todo == BLE_TODO_START;

			}
			else{
				return STATUS_INVALID_FIELD;
			}

			break;

		/******************************************************************************
		 *		PARSE EXPEDITED WRITE fields
		 ******************************************************************************/
		case BLE_EXPEDITED_WRITE:
			if(msg_status->msg_todo == BLE_TODO_FIELD1){
				/* field1 is NodeID */
				msg_status->parsed.nodeID = (uint16_t) decode_ascii_number(field_start, field_len);
				msg_status->msg_todo = BLE_TODO_FIELD2;

			}
			else if(msg_status->msg_todo == BLE_TODO_FIELD2){
				/* field2 is Index */
				msg_status->parsed.index = (uint16_t) decode_ascii_number(field_start, field_len);
				msg_status->msg_todo = BLE_TODO_FIELD3;

			}
			else if(msg_status->msg_todo == BLE_TODO_FIELD3){
				/* field3  is Sub */
				msg_status->parsed.sub = (uint16_t) decode_ascii_number(field_start, field_len);
				msg_status->msg_todo = BLE_TODO_FIELD4;

			}
			else if(msg_status->msg_todo == BLE_TODO_FIELD4){
				/* field3  is Len */
				msg_status->parsed.data_len = (uint16_t) decode_ascii_number(field_start, field_len);
				msg_status->msg_todo = BLE_TODO_C_RETURN;

			}
			else if(msg_status->msg_todo == BLE_TODO_C_RETURN){
				/* field5 (last field before '\r'  is Data */
				hex_data_decode(field_start, msg_status->parsed.data, field_len, msg_status->parsed.data_len);

			}
			else{
				return STATUS_INVALID_FIELD;
			}

			break;

		/******************************************************************************
		 *		PARSE SEGMENTED WRITE INIT fields (sdo_w_s)
		 ******************************************************************************/
		case BLE_SEGMENTED_WRITE_INIT:
			if(msg_status->msg_todo == BLE_TODO_FIELD1){
				/* field1 is NodeID */
				msg_status->parsed.nodeID = (uint16_t) decode_ascii_number(field_start, field_len);
				msg_status->msg_todo = BLE_TODO_FIELD2;

			}
			else if(msg_status->msg_todo == BLE_TODO_FIELD2){
				/* field2 is Index */
				msg_status->parsed.index = (uint16_t) decode_ascii_number(field_start, field_len);
				msg_status->msg_todo = BLE_TODO_FIELD3;
			}
			else if(msg_status->msg_todo == BLE_TODO_FIELD3){
				/* field3 is Sub */
				msg_status->parsed.sub = (uint16_t) decode_ascii_number(field_start, field_len);
				msg_status->msg_todo = BLE_TODO_C_RETURN;
			}
			else if(msg_status->msg_todo == BLE_TODO_C_RETURN){
				/* field4 (last field before '\r') is Len */
				msg_status->parsed.data_len = decode_ascii_number(field_start, field_len); // in CAN frame this goes to DATA!
			}
			else{
				return STATUS_INVALID_FIELD;
			}

			break;

		/******************************************************************************
		 *		PARSE SEGMENTED WRITE MESSAGE fields (sdo_d_w)
		 ******************************************************************************/
		case BLE_SEGMENTED_WRITE_MSG:
			if(msg_status->msg_todo == BLE_TODO_FIELD1){
				/* field1 is Len */
				msg_status->parsed.data_len = (uint16_t) decode_ascii_number(field_start, field_len);
				msg_status->msg_todo = BLE_TODO_C_RETURN;

			}
			else if(msg_status->msg_todo == BLE_TODO_C_RETURN){
				/* Data */
				hex_data_decode(field_start, msg_status->parsed.data, field_len, msg_status->parsed.data_len);

			}
			else{
				return STATUS_INVALID_FIELD;
			}

			break;

		/******************************************************************************
		 *		PARSE SEGMENTED READ INIT fields (sdo_r)
		 ******************************************************************************/
		case BLE_SEGMENTED_READ_INIT:
			if(msg_status->msg_todo == BLE_TODO_FIELD1){
				/* field1 is NodeID */
				msg_status->parsed.nodeID = (uint16_t) decode_ascii_number(field_start, field_len);
				msg_status->msg_todo = BLE_TODO_FIELD2;

			}
			else if(msg_status->msg_todo == BLE_TODO_FIELD2){
				/* field2 is Index */
				msg_status->parsed.index = (uint16_t) decode_ascii_number(field_start, field_len);
				msg_status->msg_todo = BLE_TODO_C_RETURN;

			}
			else if(msg_status->msg_todo == BLE_TODO_C_RETURN){
				/* field3 (last field before '\r') is Sub */
				msg_status->parsed.sub = (uint16_t) decode_ascii_number(field_start, field_len);

			}
			else{
				return STATUS_INVALID_FIELD;
			}

			break;

		/******************************************************************************
		 *		PARSE SEGMENTED READ MSG fields (sdo_d_r)
		 ******************************************************************************/
		case BLE_SEGMENTED_READ_MSG:
			/* No fields! nothing to be done */
			break;

		default:
			return STATUS_INVALID_TYPE;
			break;
	}

	return STATUS_OK;

}

uint32_t decode_ascii_number(uint8_t *in, uint16_t len){
	if(in[0] == '0' && in[1] == 'x'){	// hex
		return hex_ascii_to_uint32(in, len);
	}
	else{	//	decimal
		return dec_ascii_to_uint32(in, len);
	}
}

uint32_t hex_ascii_to_uint32(uint8_t *in, uint16_t len){
	/* IN format {0,x,1,b,c,...} */
	uint16_t i = 0;
	uint32_t val = 0;
	for(i=2;i<len && i<8;i++){
		val = val << 4;
		// get current character
		uint8_t byte = in[i];
		// transform hex character to the 4bit equivalent number, using the ascii table indexes
		if (byte >= '0' && byte <= '9') byte = byte - '0';
		else if (byte >= 'a' && byte <='f') byte = byte - 'a' + 10;
		else if (byte >= 'A' && byte <='F') byte = byte - 'A' + 10;
		else byte = 0;
		val = val | (byte & 0xF);
	}
	return val;

}

uint32_t dec_ascii_to_uint32(uint8_t *in, uint16_t len){
	uint16_t i = 0;
	uint32_t val = 0;
	for(i=0;i<len && i<8;i++){
		val = val*10; // place for another number
		// get current character
		uint8_t byte = in[i];
		// transform ascii character to equivalent number
		if (byte >= '0' && byte <= '9'){
			byte = byte - '0';
			val = val + byte;
		}

	}
	return val;

}


void hex_data_decode(uint8_t *in, uint8_t *out, uint16_t in_len, uint16_t out_len){
	uint16_t i = 0;
	uint8_t byte;
	for(i=0; i<out_len;i++){
		/* First char */
		byte = in[2*i];
		/* ASCII char -> number */
		if (byte >= '0' && byte <= '9') byte = byte - '0';
		else if (byte >= 'a' && byte <='f') byte = byte - 'a' + 10;
		else if (byte >= 'A' && byte <='F') byte = byte - 'A' + 10;
		else byte = 0;
		out[i] = byte<<4;

		/* Second char */
		byte = in[2*i+1];
		if (byte >= '0' && byte <= '9') byte = byte - '0';
		else if (byte >= 'a' && byte <='f') byte = byte - 'a' + 10;
		else if (byte >= 'A' && byte <='F') byte = byte - 'A' + 10;
		else byte = 0;
		out[i] = out[i] | byte; // add second character
	}
}



