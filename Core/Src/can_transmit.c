/*
 * can_transmit.c
 *
 *  Created on: Jun 29, 2021
 *      Author: nprimc
 */

#include "can_transmit.h"
#include "stdio.h" // for sprintf

extern QueueHandle_t can_txQHandle;
extern QueueHandle_t uart_txQHandle;

extern CAN_HandleTypeDef hcan;

can_status g_can;

void can_transmit(void * arg){
	parsed_msg parsed;
//	uint8_t msg[100] = {0};
	uint8_t payload[8];

	/* Variables used for UART testing */
//	uart_tx_struct tx;
//	tx.data_start = msg;

	CAN_TxHeaderTypeDef can_header;

	can_header.StdId = 0x127;  		// FIXED CAN ID of Prupulsion module
	can_header.ExtId = 0;
	can_header.IDE = CAN_ID_STD;	// use standard 11bit identifier
	can_header.DLC = 8;				// payload is always 8B
	can_header.RTR = CAN_RTR_DATA;
	can_header.TransmitGlobalTime = DISABLE;

	while(1){
		xQueueReceive(can_txQHandle, &parsed, portMAX_DELAY);
		/* All data for CAN open frame is contained in "parsed" */
		create_CAN_payload(&parsed, &g_can, payload);
		CAN_tx(&can_header, payload);
	}
}

void CAN_tx(CAN_TxHeaderTypeDef *can, uint8_t *payload){
	uint32_t mailbox;
	if(HAL_CAN_AddTxMessage(&hcan, can, payload, &mailbox) != HAL_OK){
		/* TODO Error handling */
	}
	else{
		/* Polling for  succesful transmission */
		while(HAL_CAN_IsTxMessagePending(&hcan, mailbox));
	}


}

void create_CAN_payload(parsed_msg *parsed, can_status *can, uint8_t *payload_arr){
	uint8_t cmd, i, n;
	can->type_sent = parsed->msg_type;
	switch (parsed->msg_type) {
		case BLE_EXPEDITED_READ:							//sdo_r_e
			cmd = 0x40;
			payload_arr[0] = cmd; 							//CMD
			payload_arr[1] = parsed->index & 0x00FF; 		// Index lower byte (little endian)
			payload_arr[2] = (parsed->index & 0xFF00) >> 8;	// Index upper byte
			payload_arr[3] = parsed->sub;
			payload_arr[4] = 0x00;							// data bytes are 0, present
			payload_arr[5] = 0x00;
			payload_arr[6] = 0x00;
			payload_arr[7] = 0x00;
			break;

		case BLE_EXPEDITED_WRITE:							// sdo_w_e
			if(parsed->data_len < 4){
				n = 4-parsed->data_len;
				cmd = (0x23 | (n << 2));					//s and e bits are set, n contains number of useless bytes
			}
			else{
				cmd = 0x22;									//e bit is set (all 4 bytes are used)
			}
			payload_arr[0] = cmd; 							//CMD
			payload_arr[1] = parsed->index & 0x00FF; 		// Index lower byte (little endian)
			payload_arr[2] = (parsed->index & 0xFF00) >> 8;	// Index upper byte
			payload_arr[3] = parsed->sub;
			for(i=0;i<4;i++){
				if(i<parsed->data_len) payload_arr[4+i] = parsed->data[i];	// parsed data is not zeroed
				else payload_arr[4+i] = 0x00;				// send zeros for useless bytes
			}
			break;

		case BLE_SEGMENTED_READ_INIT:
			cmd = 0x40;										// SAME CMD AS EXPEDITED!!!
			payload_arr[0] = cmd; 							// CMD
			payload_arr[1] = parsed->index & 0x00FF; 		// Index lower byte (little endian)
			payload_arr[2] = (parsed->index & 0xFF00) >> 8;	// Index upper byte
			payload_arr[3] = parsed->sub;
			payload_arr[4] = 0x00;							// data bytes are 0, present
			payload_arr[5] = 0x00;
			payload_arr[6] = 0x00;
			payload_arr[7] = 0x00;
			can->toggle = 0; //toggle begins with 0

			break;
		case BLE_SEGMENTED_READ_MSG:
			cmd = 0x60 | (can->toggle <<4);
			/* toggle the toggle bit */
			can->toggle ^= 1;
			payload_arr[0] = cmd;
			for(i=1; i<8;i++) payload_arr[i] = 0;			// All other bytes are 0

			/* EXPECTED RESPONSE */

			break;
		case BLE_SEGMENTED_WRITE_INIT:
			cmd = 0x21;
			payload_arr[0] = cmd; 							//CMD
			payload_arr[1] = parsed->index & 0x00FF; 		// Index lower byte (little endian)
			payload_arr[2] = (parsed->index & 0xFF00) >> 8;	// Index upper byte
			payload_arr[3] = parsed->sub;
			payload_arr[4] =  0x000000FF & parsed->data_len;			// data is LEN and little endian
			payload_arr[5] = (0x0000FF00 & parsed->data_len) >> 8;
			payload_arr[6] = (0x00FF0000 & parsed->data_len) >> 16;
			payload_arr[7] = (0xFF000000 & parsed->data_len) >> 24;
			can->toggle = 0;									// toggle bit starts with 0
			break;

		case BLE_SEGMENTED_WRITE_MSG:
			// LEN is sent for every block of 7B (contained in parsed.data_len)
			cmd = 0x00;
			cmd = can->toggle << 4;
			if(parsed->data_len < 7){ 		// not a full block
				n = 7 - parsed->data_len;	// n == number of useless fields
				cmd |= n << 1;
				cmd |= 0x01;				// c bit is set if there is no more data
			}
			payload_arr[0] = cmd;
			for(i=0;i<7;i++){
				if(i<parsed->data_len) payload_arr[1+i] = parsed->data[i];
				else payload_arr[1+i] = 0x00;				// send zeros for useless bytes
			}
			can->toggle ^= 1;				// toggle the toggle bit
			break;

		default:
			break;
	}
}


