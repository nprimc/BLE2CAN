/*
 * can_receive.c
 *
 *  Created on: Jul 2, 2021
 *      Author: nprimc
 */

#include "can_receive.h"
#include "cmsis_os.h"
#include "ble_receive.h"
#include "can_transmit.h"

#include "stdio.h"
#include "string.h"

extern QueueHandle_t can_rxQHandle;
extern can_status g_can;
extern UART_HandleTypeDef huart1;

static const uint32_t error_nums[] = {
        0x05030000, //"SDO toggle bit error",
        0x05040000, //"Timeout of transfer communication detected",
        0x05040001, //"Unknown SDO command specified",
        0x05040002, //"Invalid block size",
        0x05040003, //"Invalid sequence number",
        0x05040004, //"CRC error",
        0x05040005, //"Out of memory",
        0x06010000, //"Unsupported access to an object",
        0x06010001, //"Attempt to read a write only object",
        0x06010002, //"Attempt to write a read only object",
        0x06020000, //"Object does not exist",
        0x06040041, //"Object cannot be mapped to the PDO",
        0x06040042, //"PDO length exceeded",
        0x06040043, //"General parameter incompatibility reason",
        0x06040047, //"General internal incompatibility in the device",
        0x06060000, //"Access failed due to a hardware error",
        0x06070010, //"Data type and length code do not match",
        0x06070012, //"Data type does not match, length of service parameter too high",
        0x06070013, //"Data type does not match, length of service parameter too low",
        0x06090011, //"Subindex does not exist",
        0x06090030, //"Value range of parameter exceeded",
        0x06090031, //"Value of parameter written too high",
        0x06090032, //"Value of parameter written too low",
        0x06090036, //"Maximum value is less than minimum value",
        0x060A0023, //"Resource not available",
        0x08000000, //"General error",
        0x08000020, //"Data cannot be transferred or stored to the application",
        0x08000021, //"Data can not be transferred or stored to the application because of local control"
        0x08000022, //"Data can not be transferred or stored to the application because of the present device state"
        0x08000023, //"Object dictionary dynamic generation fails or no object dictionary is present"
        0x08000024  //"No data available"
    };

/* Strings that are sent to BLE in case of an error */
static const char * error_stirngs[] = {
		"SDO toggle bit error",
		"Timeout of transfer communication detected",
		"Unknown SDO command specified",
		"Invalid block size",
		"Invalid sequence number",
		"CRC error",
		"Out of memory",
		"Unsupported access to an object",
		"Attempt to read a write only object",
		"Attempt to write a read only object",
		"Object does not exist",
		"Object cannot be mapped to the PDO",
		"PDO length exceeded",
		"General parameter incompatibility reason",
		"General internal incompatibility in the device",
		"Access failed due to a hardware error",
		"Data type and length code do not match",
		"Data type does not match, length of service parameter too high",
		"Data type does not match, length of service parameter too low",
		"Subindex does not exist",
		"Value range of parameter exceeded",
		"Value of parameter written too high",
		"Value of parameter written too low",
		"Maximum value is less than minimum value",
		"Resource not available",
		"General error",
		"Data cannot be transferred or stored to the application",
		"Data can not be transferred or stored to the application because of local control",
		"Data can not be transferred or stored to the application because of the present device state",
		"Object dictionary dynamic generation fails or no object dictionary is present",
		"No data available"
};


void can_receive(void * arg){

	can_rx_struct can_rx;
	uint16_t valid_data_len, i;
	uint32_t errno;
	uint8_t data_string[16], final; //2*7+1 ('0')
	char msg[25];
	uint8_t test_data[] = {0x17,0x12,0x13,0x11,0x1A,0x15,0x22, 0x5B};

	can_rx.data = test_data;
	g_can.toggle = 1;
	g_can.type_sent = BLE_SEGMENTED_READ_MSG;

	while(1){
		xQueueReceive(can_rxQHandle, &can_rx, portMAX_DELAY);
		switch (g_can.type_sent) {
			case BLE_EXPEDITED_READ:
				if((can_rx.data[0] & 0x42) == 0x42){			// CMD byte: sdo_download + expedited
					/* CMD(0) == s bit, if set data length is given in 'n' bits */
					if(can_rx.data[0] & 0x01){
						valid_data_len = 4 - ((can_rx.data[0] & 0x0C) >> 2);
						/* Send data */
						data_to_string(valid_data_len, &(can_rx.data[4]), data_string);
						sprintf(msg, "OK 1,%d,%s\r\n",valid_data_len, data_string);
						/* Get message length */
						i = 0;
						while(msg[i]){
								i++;
						}
						/* Transmit to UART (BLE) */
						HAL_UART_Transmit(&huart1, (uint8_t *)(msg), i, 100);
					}
					/* No data length is given, all 4 bytes are valid */
					else{
						valid_data_len = 4;
						data_to_string(valid_data_len, &(can_rx.data[4]), data_string);
						sprintf(msg, "OK 1,%d,%s\r\n",valid_data_len, data_string);
						i = 0;
						while(msg[i]){
								i++;
							}
						/* Transmit to UART (BLE) */
						HAL_UART_Transmit(&huart1, (uint8_t *)(msg), i, 100);
					}
				}
				else if (can_rx.data[0] == 0x80) {		// CMD = Error
					errno = bytes_to_error_num(&(can_rx.data[4]));
					/* Transmit to UART (BLE) */
					uart_send_error(errno);
				}
				break;
			case BLE_EXPEDITED_WRITE:
				if(can_rx.data[0] == 0x60){
					/* Transmit to UART (BLE) */
					uart_send_OK();
				}
				else if (can_rx.data[0] == 0x80) {		// CMD = Error
					errno = bytes_to_error_num(&(can_rx.data[4]));
					/* Transmit to UART (BLE) */
					uart_send_error(errno);
				}

				break;
			case BLE_SEGMENTED_READ_INIT:
				if(can_rx.data[0] == 0x41){
					/* Transmit to UART (BLE) */
					uart_send_OK();
				}
				else if (can_rx.data[0] == 0x80) {		// CMD = Error
					errno = bytes_to_error_num(&(can_rx.data[4]));
					/* Transmit to UART (BLE) */
					uart_send_error(errno);
				}

				break;
			case BLE_SEGMENTED_READ_MSG:
				if((can_rx.data[0] & g_can.toggle << 4) == g_can.toggle << 4){			// CMD byte - check toggle
					final = (can_rx.data[0] & 0x1); // final packet is indicated in CMD byte bit 0
					if(final){
						valid_data_len = 7 - ((0xE & can_rx.data[0])>>1);
						data_to_string(valid_data_len, &(can_rx.data[1]), data_string); // data starts at payload[1]
						sprintf(msg, "OK %d,%d,%s\r\n",final, valid_data_len, data_string);
						i = 0;
						while(msg[i]){
								i++;
							}
						HAL_UART_Transmit(&huart1, (uint8_t *)(msg), i, 100);
					}
					else{
						valid_data_len = 7;
						data_to_string(valid_data_len, &(can_rx.data[1]), data_string); // data starts at payload[1]
						sprintf(msg, "OK %d,%d,%s\r\n",final, valid_data_len, data_string);
						i = 0;
						while(msg[i]){
								i++;
							}
						HAL_UART_Transmit(&huart1, (uint8_t *)(msg), i, 100);
					}

				}
				else if (can_rx.data[0] == 0x80) {		// CMD = Error
					errno = bytes_to_error_num(&(can_rx.data[4]));
					uart_send_error(errno);
				}

				break;
			case BLE_SEGMENTED_WRITE_INIT:
				if(can_rx.data[0] == 0x60){				// CMD byte
					uart_send_OK();
				}
				else if (can_rx.data[0] == 0x80) {		// CMD = Error
					errno = bytes_to_error_num(&(can_rx.data[4]));
					uart_send_error(errno);
				}

				break;
			case BLE_SEGMENTED_WRITE_MSG:
				if(can_rx.data[0] == (0x20 | g_can.toggle << 4)){			// CMD byte - check toggle bit
					uart_send_OK();
				}
				else if (can_rx.data[0] == 0x80) {		// CMD = Error
					errno = bytes_to_error_num(&(can_rx.data[4]));
					uart_send_error(errno);
				}

				break;
			default:
				break;
		}

	}
}

void uart_send_OK(void){
	uint8_t msg[] = "OK\r\n";
	HAL_UART_Transmit(&huart1, (msg), 4, 100);
}


void uart_send_error(uint32_t error_num){
	uint16_t i = 0;
	char msg[110];
	for(i=0; i<32; i++){ // there is 31 emergency codes
		if(error_nums[i] == error_num){
			break;
		}
	}
	if(i > 30){	// Error num from CAN is unknown -> SEnd general error string
		i = 25;
	}
	sprintf(msg, "ERR %lX,%s\r\n", error_num, error_stirngs[i]);
	/* Message length */
	i = 0;
	while(msg[i]){
		i++;
	}
	HAL_UART_Transmit(&huart1, (uint8_t *)(msg), i, 100);
}

uint32_t bytes_to_error_num(uint8_t *data){
	return (data[0]<<24)|(data[1]<<16)|(data[2]<<8)|(data[3]);
}

void data_to_string(uint16_t valid_len, uint8_t *data, uint8_t *string){
	uint16_t i;
	string[0] = 0;
	char tmp[3];
	for(i=0; i<valid_len; i++){
		sprintf(tmp, "%X", data[i]);
		strcat((char *) string, tmp);
	}
}


